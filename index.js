const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/user')

mongoose.connection.once('open', () => console.log('Now connected to local MongoDB server.'))
mongoose.connect('', { 
    useNewUrlParser: true, 
    useUnifiedTopology: true 
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors())
app.use('/api/users', userRoutes)

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})